module Main where

import Prelude

import Amazing (amazing)
import Effect (Effect)
import Effect.Console (log)

main :: Effect Unit
main = do
  log $ amazing "test"
